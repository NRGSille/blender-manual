
***********
Comb Curves
***********

Shape the curves by moving their control points while preserving the initial length of every curve segment.


Brush Settings
==============

.. _bpy.ops.brush.sculpt_curves_falloff_preset:

Curve Falloff
   Falloff that is applied from the tip to the root of each curve.
